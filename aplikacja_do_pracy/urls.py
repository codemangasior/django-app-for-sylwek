from django.contrib import admin
from django.urls import path, include
from do_pracy import views


urlpatterns = [
    path("do_pracy/", include("do_pracy.urls")),
    path("admin/", admin.site.urls),
    path("", views.IndexView.as_view(), name="welcome"),
]
