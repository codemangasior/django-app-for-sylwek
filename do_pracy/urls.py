from django.urls import path
from .views import HomeView, RecordView
from django.conf import settings
from django.conf.urls.static import static


app_name = "do_pracy"
urlpatterns = [
    path("", HomeView.as_view(), name="index"),
    path("record", RecordView.as_view(), name="record"),
]
