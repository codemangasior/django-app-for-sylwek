# Generated by Django 2.1.5 on 2019-01-29 18:03

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [("do_pracy", "0021_post_profile_photo")]

    operations = [
        migrations.AddField(
            model_name="post",
            name="link",
            field=models.CharField(default=django.utils.timezone.now, max_length=200),
            preserve_default=False,
        )
    ]
