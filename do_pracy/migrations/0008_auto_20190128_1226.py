# Generated by Django 2.1.5 on 2019-01-28 11:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [("do_pracy", "0007_auto_20190128_1222")]

    operations = [
        migrations.CreateModel(
            name="Rekra",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("first_name", models.CharField(max_length=50)),
                ("last_name", models.CharField(max_length=100)),
                ("pub_date", models.DateTimeField(verbose_name="datepublished")),
                (
                    "ident",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="do_pracy.Ident"
                    ),
                ),
            ],
        ),
        migrations.RemoveField(model_name="rec", name="ident"),
        migrations.DeleteModel(name="Rec"),
    ]
