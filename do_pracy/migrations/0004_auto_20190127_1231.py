# Generated by Django 2.1.5 on 2019-01-27 11:31

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [("do_pracy", "0003_auto_20190127_1032")]

    operations = [
        migrations.DeleteModel(name="Cv"),
        migrations.DeleteModel(name="First_name"),
        migrations.DeleteModel(name="Last_name"),
        migrations.DeleteModel(name="Profile_photo"),
    ]
