# Generated by Django 2.1.5 on 2019-01-27 18:58

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [("do_pracy", "0005_rec")]

    operations = [
        migrations.AddField(
            model_name="rec",
            name="pub_date",
            field=models.DateTimeField(
                default=django.utils.timezone.now, verbose_name="datepublished"
            ),
            preserve_default=False,
        )
    ]
