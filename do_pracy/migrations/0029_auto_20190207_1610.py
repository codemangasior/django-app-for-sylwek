# Generated by Django 2.1.5 on 2019-02-07 15:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("do_pracy", "0028_remove_post_user")]

    operations = [
        migrations.AlterField(
            model_name="post",
            name="date",
            field=models.DateTimeField(auto_now_add=True),
        )
    ]
