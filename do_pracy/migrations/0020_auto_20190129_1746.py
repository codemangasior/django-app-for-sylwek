# Generated by Django 2.1.5 on 2019-01-29 16:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [("do_pracy", "0019_post_file")]

    operations = [
        migrations.RemoveField(model_name="post", name="cv"),
        migrations.RemoveField(model_name="post", name="profile_photo"),
    ]
