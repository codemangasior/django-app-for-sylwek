from django.db import models
from django.contrib.auth.models import User


class Post(models.Model):
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=50)
    date = models.DateTimeField(auto_now_add=True)
    file = models.CharField(max_length=200)
    profile_photo = models.CharField(max_length=200)

    def __str__(self):
        return self.first_name
