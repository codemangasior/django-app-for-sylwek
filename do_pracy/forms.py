from django import forms
from do_pracy.models import Post


class HomeForm(forms.ModelForm):
    first_name = forms.CharField(required=True)

    class Meta:
        model = Post
        fields = ("first_name", "last_name", "file", "profile_photo")
