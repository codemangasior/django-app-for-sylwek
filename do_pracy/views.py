from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView
from do_pracy.forms import HomeForm
from .models import Post


class HomeView(TemplateView):
    template_name = "base.html"

    def get(self, request):
        form = HomeForm()
        posts = Post.objects.all()
        args = {"form": form, "posts": posts}

        return render(request, self.template_name, args)

    def post(self, request):
        form = HomeForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.user = request.user
            post.save()

            return HttpResponseRedirect("record")


class RecordView(TemplateView):
    template_name = "do_pracy/record.html"

    def get(self, request):
        posts = Post.objects.all()

        for post in posts:
            first_name = post.first_name
            last_name = post.last_name
            date = post.date
            file = post.file
            photo_link = post.profile_photo[:-1].rpartition("/")

        args = {
            "posts": posts,
            "photo_link": photo_link[0],
            "first_name": first_name,
            "last_name": last_name,
            "date": date,
            "file": file,
        }

        return render(request, self.template_name, args)


class IndexView(TemplateView):
    template_name = "welcome.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["latest_articles"] = Post.objects.all()[:5]

        return context
